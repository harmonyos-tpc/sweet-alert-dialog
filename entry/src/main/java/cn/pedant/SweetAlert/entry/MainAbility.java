package cn.pedant.SweetAlert.entry;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.utils.IntervalTimer;

public class MainAbility extends Ability {
    private int index = -1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_basic_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                SweetAlertDialog sd = new SweetAlertDialog(getContext());
                                sd.siteRemovable(true);
                                sd.setAutoClosable(true);
                                sd.show();
                            }
                        });
        findComponentById(ResourceTable.Id_under_text_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext()).setContentText("It,s pretty,isn't it?").show();
                            }
                        });
        findComponentById(ResourceTable.Id_error_text_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Something went wrong!")
                                        .show();
                            }
                        });
        findComponentById(ResourceTable.Id_success_text_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Goog job!")
                                        .setContentText("You clicked the button")
                                        .show();
                            }
                        });
        findComponentById(ResourceTable.Id_warning_confirm_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure?")
                                        .setContentText("Won't be able to recover this file")
                                        .setConfirmText("yes.delete it!")
                                        .setConfirmClickListener(
                                                new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        sweetAlertDialog
                                                                .setTitleText("Deleted!")
                                                                .setContentText("Your imainary file has been deleted!")
                                                                .setConfirmText("OK")
                                                                .setConfirmClickListener(null)
                                                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                    }
                                                })
                                        .show();
                            }
                        });
        findComponentById(ResourceTable.Id_warning_cancel_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Are you sure?")
                                        .setContentText("Won't be able to recover this file")
                                        .setCancelText("No,cancel plx!")
                                        .setConfirmText("Yes,delete it!")
                                        .showCancelButton(true)
                                        .setCancelClickListener(
                                                new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        sweetAlertDialog
                                                                .setTitleText("Cancelled!")
                                                                .setContentText("Yout imainary file is safe :)")
                                                                .setConfirmText("OK")
                                                                .showCancelButton(false)
                                                                .setCancelClickListener(null)
                                                                .setConfirmClickListener(null)
                                                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                                    }
                                                })
                                        .setConfirmClickListener(
                                                new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        sweetAlertDialog
                                                                .setTitleText("Deleted!")
                                                                .setContentText("Your imaginary file has been deleted!")
                                                                .setConfirmText("OK")
                                                                .showCancelButton(false)
                                                                .setCancelClickListener(null)
                                                                .setConfirmClickListener(null)
                                                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                    }
                                                })
                                        .show();
                            }
                        });
        findComponentById(ResourceTable.Id_custom_img_test)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SweetAlertDialog(getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText("Sweet!")
                                        .setContentText("Here's a custom image.")
                                        .setCustomImage(ResourceTable.Media_custom_img)
                                        .show();
                            }
                        });

        findComponentById(ResourceTable.Id_progress_dialog)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                final SweetAlertDialog pDialog =
                                        new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE)
                                                .setTitleText("Loading");
                                pDialog.show();
                                pDialog.siteRemovable(false);
                                new IntervalTimer(800 * 7, 800) {
                                    @Override
                                    public void onInterval(long l) {
                                        index++;
                                        switch (index) {
                                            case 0:
                                                pDialog.getProgressHelper().setBarColor(0xffAEDEF4);
                                                break;
                                            case 1:
                                                pDialog.getProgressHelper().setBarColor(0xff009688);
                                                break;
                                            case 2:
                                                pDialog.getProgressHelper().setBarColor(0xffA5DC86);
                                                break;
                                            case 3:
                                                pDialog.getProgressHelper().setBarColor(0xff80cbc4);
                                                break;
                                            case 4:
                                                pDialog.getProgressHelper().setBarColor(0xff37474f);
                                                break;
                                            case 5:
                                                pDialog.getProgressHelper().setBarColor(0xffF8BB86);
                                                break;
                                            case 6:
                                                pDialog.getProgressHelper().setBarColor(0xffCD5B55);
                                                break;
                                        }
                                    }

                                    public void onFinish() {
                                        index = -1;
                                        pDialog.setTitleText("Success!")
                                                .setConfirmText("OK")
                                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                    }
                                }.schedule();
                            }
                        });
    }
}
