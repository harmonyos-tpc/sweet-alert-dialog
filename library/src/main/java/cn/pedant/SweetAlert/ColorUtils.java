/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.pedant.SweetAlert;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

/**
 * get blend color
 */
public class ColorUtils {
    /**
     * get the path from id
     *
     * @param background the background color
     * @param foreground the background color
     * @param alpha the foreground alpha
     * @return the blend color
     */
    public static int cpc(int background, int foreground, float alpha) {
        RgbColor backgroundColor = RgbColor.fromArgbInt(background);
        RgbColor foregroundColor = RgbColor.fromArgbInt(foreground);
        float red = foregroundColor.getRed() * alpha + backgroundColor.getRed() * (1 - alpha);
        float green = foregroundColor.getGreen() * alpha + backgroundColor.getGreen() * (1 - alpha);
        float blue = foregroundColor.getBlue() * alpha + backgroundColor.getBlue() * (1 - alpha);
        return Color.rgb((int) red, (int) green, (int) blue);
    }

    /**
     * get the path from id
     *
     * @param background the background color
     * @param foreground the background color
     * @return the blend color
     */
    public static int cpc(int background, int foreground) {
        RgbColor backgroundColor = RgbColor.fromArgbInt(background);
        RgbColor foregroundColor = RgbColor.fromArgbInt(foreground);
        float alpha = foregroundColor.getAlpha() / 255.0f;
        float red = foregroundColor.getRed() * alpha + backgroundColor.getRed() * (1 - alpha);
        float green = foregroundColor.getGreen() * alpha + backgroundColor.getGreen() * (1 - alpha);
        float blue = foregroundColor.getBlue() * alpha + backgroundColor.getBlue() * (1 - alpha);
        return Color.rgb((int) red, (int) green, (int) blue);
    }
}
