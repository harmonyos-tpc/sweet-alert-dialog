/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.pedant.SweetAlert;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * the warning view
 */
public class WarningView extends Component {
    private Paint mPaint;
    private Paint dPaint;
    private int boundWidth;
    private int boundHeight;
    private int strokeWidth = dp2px(3);

    public WarningView(Context context) {
        this(context, null, null);
    }

    public WarningView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public WarningView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(new Color(0xffF8BB86));
        mPaint.setStrokeWidth(strokeWidth);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        dPaint = new Paint();
        dPaint.setAntiAlias(true);
        dPaint.setColor(new Color(0xffF8BB86));
        onDraw();
    }

    private void onDraw(){
        addDrawTask(
                (component, canvas) -> {
                    boundWidth = getWidth() - getPaddingLeft() - getPaddingRight();
                    boundHeight = getWidth() - getPaddingTop() - getPaddingBottom();
                    if (boundWidth > boundHeight) {
                        boundHeight = boundWidth;
                    } else {
                        boundWidth = boundHeight;
                    }
                    canvas.drawCircle(
                            boundWidth / 2.0f, boundHeight / 2.0f, boundWidth / 2.0f - strokeWidth / 2.0f, mPaint);

                    canvas.drawLine(
                            new Point(boundWidth / 2.0f, (boundHeight - dp2px(35)) / 2.0f),
                            new Point(boundWidth / 2.0f, (boundHeight - dp2px(35)) / 2.0f + dp2px(22)),
                            mPaint);
                    canvas.drawOval(
                            new RectFloat(
                                    boundWidth / 2.0f - dp2px(2),
                                    (boundHeight - dp2px(35)) / 2.0f + dp2px(30),
                                    boundWidth / 2.0f + dp2px(2),
                                    (boundHeight - dp2px(35)) / 2.0f + dp2px(35)),
                            dPaint);
                });
    }

    private int dp2px(float dp) {
        return (int) (getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
